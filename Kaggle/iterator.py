import multiprocessing
import threading
import Queue
from uuid import uuid4

import numpy as np
import SharedArray

import random
from PIL import Image, ImageEnhance
from skimage.transform import warp, AffineTransform

img_h, img_w = 512, 512

def random_perturbation(img):
    rescale_factor = 1.2
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape

def load_augmented(filenames, normalize=None, augment=True):
    # filenames : list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        h, w, d = img.shape
        if augment:
            # random color, contrast, brightness perturbation
            img = random_perturbation(img)
            
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
           
            # affine transform (scale, rotation)
            shift_y, shift_x = np.array(img.shape[:2]) / 2.
            shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
            shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
            r_angle = random.randint(0, 359)
            scale_factor = 1.15
            scale = random.uniform(1. / scale_factor, scale_factor)
            tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
            img = warp(img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255

        if normalize == "normalize":
            images_arr[file_index] = 1.*img / 255.0
        else:
            images_arr[file_index] = img.astype(np.float32)
        
    return images_arr


def load_shared(args):
    i, img_array_name, normalize, augment, fname = args
    img_array = SharedArray.attach(img_array_name)
    img_array[i] = load_augmented([fname], normalize, augment)


def balance_per_class_indices(y, weights):
    weights = np.array(weights, dtype=float)
    p = np.zeros(len(y))
    for i, weight in enumerate(weights):
        p[y == i] = weight
    return np.random.choice(np.arange(len(y)), size=len(y), replace=True,
                            p=np.array(p) / p.sum())


class BatchIterator(object):

    def __call__(self, *args):
        self.X, self.y = args
        return self
        
    def __iter__(self):
        n_samples = self.X.shape[0]
        bs = self.batch_size
        for i in range((n_samples + bs - 1) // bs - 1):
            sl = slice(i * bs, (i + 1) * bs)
            X = self.X[sl]
            y = self.y[sl]
            yield self.transform(X, y)


class QueueIterator(BatchIterator):

    def __iter__(self):
        queue = Queue.Queue(maxsize=20)
        end_marker = object()

        def producer():
            for filenames, Xb, yb in super(QueueIterator, self).__iter__():
                queue.put((np.array(filenames), np.array(Xb), np.array(yb)))
            queue.put(end_marker)
        
        thread = threading.Thread(target=producer)
        thread.daemon = True
        thread.start()

        item = queue.get()
        while item is not end_marker:
            yield item
            queue.task_done()
            item = queue.get()


class SharedIterator(QueueIterator):

    def __init__(self):
        self.pool = multiprocessing.Pool()

    def transform(self, fnames, labels):
        img_shared_array_name = str(uuid4())
        try:
            img_shared_array = SharedArray.create(
                img_shared_array_name, [len(fnames), img_h, img_w, 3], dtype=np.float32)
                                        
            args = []
            
            for i, fname in enumerate(fnames):
                args.append((i, img_shared_array_name, self.normalize, self.augment, fname))

            self.pool.map(load_shared, args)
            imgs = np.array(img_shared_array, dtype=np.float32)
            
        finally:
            SharedArray.delete(img_shared_array_name)
            
        return fnames, imgs, labels

    
class TrainBatchFetcher(SharedIterator):

    def __init__(self, train_filenames_labels, batch_size, weight, normalize):
        self.train_files = np.array(train_filenames_labels.filename.tolist())
        self.train_labels = np.array(train_filenames_labels.label.tolist())
        self.weight = np.array(weight)
        self.normalize = normalize
        self.augment = True
        self.batch_size = batch_size
        super(TrainBatchFetcher, self).__init__()
        
    def __call__(self):
        indices = balance_per_class_indices(self.train_labels, weights=self.weight)
        X = self.train_files[indices]
        y = self.train_labels[indices]
        return super(TrainBatchFetcher, self).__call__(X, y)


class ValidationBatchFetcher(SharedIterator):

    def __init__(self, validation_filenames_labels, batch_size, normalize):
        self.validation_files = np.array(validation_filenames_labels.filename.tolist())
        self.validation_labels = np.array(validation_filenames_labels.label.tolist())
        self.normalize = normalize
        self.augment = False
        self.batch_size = batch_size
        super(ValidationBatchFetcher, self).__init__()
        
    def __call__(self):
        return super(ValidationBatchFetcher, self).__call__(self.validation_files, self.validation_labels)

