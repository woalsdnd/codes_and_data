import os

from keras import backend as K
from keras import objectives
from keras.layers import Input, Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense, AveragePooling2D, UpSampling2D, Flatten, Multiply, RepeatVector
from keras.models import Model
from keras import regularizers, initializers
from keras.optimizers import Adam, SGD
from keras.layers.advanced_activations import ELU
from keras.layers.core import Lambda, Activation
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import Concatenate, Add
import tensorflow as tf

os.environ['KERAS_BACKEND'] = 'tensorflow'
K.set_image_dim_ordering('tf')


def set_optimizer(network, method=None):

    def L2(y_true, y_pred):
        y_pred_clipped = K.clip(y_pred, 0, 1)
        return objectives.mean_squared_error(y_true, y_pred_clipped)

    def joint_training_loss(y_true, y_pred):
        y_pred_clipped = K.clip(y_pred, 0, 1)
        L2 = objectives.mean_squared_error(y_true, y_pred_clipped)
        entropy = -K.sum(y_pred_clipped * K.log(K.maximum(y_pred_clipped, K.epsilon())) + (1 - y_pred_clipped) * K.log(K.maximum(1 - y_pred_clipped, K.epsilon())), axis=-1)
        return L2 + 0.2 * entropy 

    if method is None:
        network.compile(optimizer=Adam(lr=1e-4), loss=L2, metrics=['accuracy'])
    elif method == "joint_training":
        network.compile(optimizer=Adam(lr=1e-4), loss=joint_training_loss, metrics=['accuracy'])
    return network


def conv_block(x, n_filters, filter_size, strides, padding, l2_coeff):
    conv = Conv2D(n_filters, filter_size, strides=strides, padding=padding,
                  kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    conv = BatchNormalization(scale=False, axis=3)(conv)
    conv = Activation('relu')(conv) 
    return conv


def resnet_buliding_block(x, n_filters, filter_size, padding, l2_coeff):
    conv1 = Conv2D(n_filters, filter_size, padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)   

    conv2 = Conv2D(n_filters, filter_size, padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Add()([conv2, x])
    
    out = Activation('relu')(conv2)   
    
    return out


def resnet_blocks(x, n, n_filters, filter_size, padding, l2_coeff):
    for _ in range(n):
        x = resnet_buliding_block(x, n_filters, filter_size, padding, l2_coeff)
    return x


def network_activation_out(img_shape):

    # set image specifics
    n_filters = 32
    out_ch = 1
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0
    list_n_building_blocks = [2, 3, 4]
    blocks = []
    
    inputs = Input((img_h, img_w, img_ch))
    blocks.append(conv_block(inputs, n_filters, (k, k), (s, s), padding, l2_coeff))
    for index, n_building_blocks in enumerate(list_n_building_blocks):
        blocks.append(resnet_blocks(blocks[index], n_building_blocks, (2 ** index) * n_filters, (k, k), padding, l2_coeff))
        blocks[index + 1] = conv_block(blocks[index + 1], 2 ** (index + 1) * n_filters, (k, k), (s, s), padding, l2_coeff)
    
    list_avg_pools = []
    for i in range(3):
        list_avg_pools.append(AveragePooling2D((8 // (2 ** i), 8 // (2 ** i)))(blocks[i]))
    blocks_concat = Concatenate(axis=3)(list_avg_pools + [blocks[3]])
    list_dilated_conv = []
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(2, 2), padding=padding,
                                     kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(4, 4), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(8, 8), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    
    final_block = Concatenate(axis=3)(list_dilated_conv)
    final_block = conv_block(final_block, 16 * n_filters, (k, k), (s, s), padding, l2_coeff)
    final_result = Conv2D(out_ch, (1, 1), padding=padding)(final_block)
    gap = GlobalAveragePooling2D()(final_result)
    outputs = gap
    model = Model(inputs, outputs)    
    
    def L2(y_true, y_pred):
        y_pred_clipped = K.clip(y_pred, 0, 1)
        return objectives.mean_squared_error(y_true, y_pred_clipped)

    model.compile(optimizer=Adam(lr=1e-4), loss=L2, metrics=['accuracy'])

    return model
