import os
import argparse
import time
import sys
import iterator
import models
import numpy as np
from keras.models import model_from_json
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score

# misc params
batch_size = 28
model_dir = "pretrained"
n_epochs = 50

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--load_model_dir',
    type=str,
    required=False,
    default=None
    )
parser.add_argument(
    '--noise_ratio',
    type=float,
    required=False,
    default=0
    )
parser.add_argument(
    '--img_path',
    type=str,
    required=True,
    )
FLAGS, _ = parser.parse_known_args()

# set lambda, gpu, path
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)
if FLAGS.noise_ratio != 0:
    model_dir = "{}_r{}".format(model_dir, FLAGS.noise_ratio)
path_train = FLAGS.img_path
path_test = FLAGS.img_path

            
def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def print_stats(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    if len(y_true[y_true == 1]) > 0:
        cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
        spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        HM = 2 * spe * sen / (spe + sen)
        AUC_ROC = roc_auc_score(y_true, y_pred)
        print cm
        print "specificity : {},  sensitivity : {}, harmonic mean : {},  ROC_AUC : {} ".format(spe, sen, HM, AUC_ROC)   
    else:
        print "specificity: {}".format(1.*len(y_true))


def get_metric(metrics, y_true, y_pred):
    values = []
    for metric in metrics:
        if metric == "HM":
            cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
            spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
            sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
            HM = 2 * spe * sen / (spe + sen)
            values.append(HM)
        elif metric == "auroc":
            AUC_ROC = roc_auc_score(y_true, y_pred)
            values.append(AUC_ROC)

    return values


def split_training_set(df_labels):
    df_train = df_labels.iloc[:-24693]
    df_val = df_labels.iloc[-24693:-17638]
    df_test = df_labels.iloc[-17638:]
    return df_train, df_val, df_test


def class_weight(df):
    data_ratio = df["label"].value_counts(normalize=True)
    weight = np.array([1. / ratio for ratio in data_ratio])
    weight /= sum(weight)
    print "data number"
    print df["label"].value_counts(normalize=False)
    print "class weight : {}".format(weight)
    return weight


def assign(val):
    if val < 3:
        return 0
    elif val >= 3:
        return 1
    else:
        None


def binarize(df, istrain):
    if istrain:
        df["filename"] = df["image"].map(lambda x:os.path.join(path_train, x + ".png"))
    else:
        df["filename"] = df["image"].map(lambda x:os.path.join(path_test, x + ".png"))
    df["label"] = df["level"].map(assign)
    df["filename"] = df["filename"].map(lambda x:x if os.path.exists(x) else None)
    df.dropna(subset=["filename"], inplace=True)
    return df[["filename", "label"]]


def add_noise(df, noise_ratio):
    abnormal_indices = df.index[df['label'] == 1].tolist()
    normal_indices = df.index[df['label'] == 0].tolist()
    n_flip = int(len(abnormal_indices) * noise_ratio)
    flip_indices_abnormal = abnormal_indices[:n_flip]
    flip_indices_normal = normal_indices[:n_flip]
    print "flip {} labels".format(n_flip)
    df.loc[flip_indices_abnormal, 'label'] = 0
    df.loc[flip_indices_normal, 'label'] = 1
    return df


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    network = models.set_optimizer(network)
    return network

# make save dir
mkdir_if_not_exist(model_dir)

# training settings
filenames_labels1 = binarize(pd.read_csv("trainLabels.csv"), istrain=True)
filenames_labels2 = binarize(pd.read_csv("testLabels.csv"), istrain=False)
train_filenames_labels = pd.concat([filenames_labels1, filenames_labels2], ignore_index=True)
train_filenames_labels, val_filenames_labels, test_filenames_labels = split_training_set(train_filenames_labels)
if FLAGS.noise_ratio != 0:
    train_filenames_labels = add_noise(train_filenames_labels, FLAGS.noise_ratio)
weight = class_weight(train_filenames_labels)
train_batch_fetcher = iterator.TrainBatchFetcher(train_filenames_labels, batch_size, weight, "normalize")
validation_batch_fetcher = iterator.ValidationBatchFetcher(val_filenames_labels, batch_size, "normalize")
test_batch_fetcher = iterator.ValidationBatchFetcher(test_filenames_labels, batch_size, "normalize")
print "train: {}, val: {}, test: {}".format(len(train_filenames_labels), len(val_filenames_labels), len(test_filenames_labels))

# create & save network
if FLAGS.load_model_dir:
    network = load_network(FLAGS.load_model_dir)
    print "model loaded from files"
else:    
    network = models.network_activation_out((512, 512, 3))
    print "model from the scratch"
network.summary()
with open(os.path.join(model_dir, "network.json"), 'w') as f:
    f.write(network.to_json())

# train the network 
auroc_best = 0
for epoch in range(n_epochs):

    # train on train set
    training_loss = {"loss":[], "acc":[]}
    start_time = time.time()
    for filenames, batch_x, batch_y in train_batch_fetcher():
        loss, acc = network.train_on_batch(batch_x, batch_y)
        training_loss["loss"] += [loss] * len(filenames)
        training_loss["acc"] += [acc] * len(filenames)
    print "loss: {}".format(np.mean(training_loss["loss"]))
    print "acc: {}".format(np.mean(training_loss["acc"]))

    # evaluate on val set
    val_loss = {"loss":[], "acc":[]}
    for filenames, batch_x, batch_y in validation_batch_fetcher():
        loss, acc = network.evaluate(batch_x, batch_y, verbose=0)
        val_loss["loss"] += [loss] * len(filenames)
        val_loss["acc"] += [acc] * len(filenames)
    print "classification loss: {}".format(np.mean(val_loss["loss"]))
    print "classification acc: {}".format(np.mean(val_loss["acc"]))
    
    # evaluate on test set
    pred_labels = []
    true_labels = []
    for filenames, batch_x, batch_y in test_batch_fetcher():
        preds = network.predict(batch_x)[:, 0]
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
    print_stats(true_labels, pred_labels)
    HM_curr, auroc_curr = get_metric(["HM", "auroc"], true_labels, pred_labels)
    duration = time.time() - start_time
    print "{}th epoch ==> duration : {}".format(epoch, duration)
    sys.stdout.flush()
    
    # save weights
    network.save_weights(os.path.join(model_dir, "weight_{}epoch.h5".format(epoch)))
    if auroc_curr > auroc_best:
        auroc_best = auroc_curr
        HM_at_best_auroc = HM_curr
        network.save_weights(os.path.join(model_dir, "weight_best_auroc.h5"))
    sys.stdout.flush()
print "best auroc : {}, HM at best auroc : {}".format(auroc_best, HM_at_best_auroc)
