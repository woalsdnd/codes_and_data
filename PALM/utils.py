import os
import re

from PIL import Image, ImageEnhance

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
import random
from skimage.transform import warp, AffineTransform
from subprocess import Popen, PIPE
import math
from scipy.misc import imresize
from skimage import color
from keras import backend as K
from keras.models import model_from_json
from PIL import ImageFilter
import matplotlib.cm as cm


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def augment_img(img):
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    # random flip
    if random.getrandbits(1):
        img = img[:, ::-1, :]
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(0, 359)
    scale_factor = 1.2
    shear_degree = 10
    translation_ratio = 0.1
    x_scale = random.uniform(1, scale_factor)
    y_scale = random.uniform(1, scale_factor)
    shear = random.uniform(-shear_degree, shear_degree)
    h_trans_margin = translation_ratio * img.shape[0]
    w_trans_margin = translation_ratio * img.shape[1]
    h_translation = int(random.uniform(-h_trans_margin, h_trans_margin))
    w_translation = int(random.uniform(-w_trans_margin, w_trans_margin))
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle), shear=np.deg2rad(shear), translation=(w_translation, h_translation))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255
    return img


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def class_weight(df):
    data_ratio = df["label"].value_counts(normalize=True)
    weight = np.array([1. / data_ratio[0], 1. / data_ratio[1]])
    weight /= sum(weight)
    print ("data number")
    print (df["label"].value_counts(normalize=False))
    print ("class weight : {}".format(weight))
    return weight


def analyze_imgs(filenames, true_labels, pred_labels, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    pred_labels = outputs2labels(pred_labels, 0, 1)

    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            
            std_out, std_err = pipes.communicate()


def analyze_imgs_inference(filenames, activations, out_dir):
    mkdir_if_not_exist(out_dir)
    h_target, w_target = 512, 512
    for i in range(len(filenames)):
        img = np.array(Image.open(filenames[i])).astype(np.float32)
        activation = activations[i, :, :, 0]
        heatmap = (activation - np.min(activation)) / (np.max(activation) - np.min(activation))
        heatmap = imresize(heatmap, (h_target, w_target), 'bilinear')
        heatmap_pil_img = Image.fromarray(heatmap)
        heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(32))
        heatmap_blurred = np.asarray(heatmap_pil_img)
        heatmap_blurred = np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
        overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
        Image.fromarray(overlapped).save(os.path.join(out_dir, os.path.basename(filenames[i]).replace(".png", "") + "_superimposed.png"))


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def random_perturbation(img):
    rescale_factor = 1.5
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def load_augmented(fname, normalize, augment):
    # read image file
    img_aug = load_imgs([fname], normalize, augment)
    img_aug = img_aug[0, ...]
    return img_aug


def load_imgs(filenames, normalize=None, augment=True):
    # filenames : list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        # remove low values & erase time 
        img[img < 10] = 0
        h, w, d = img.shape

        if augment:
            # random color, contrast, brightness perturbation
            img = random_perturbation(img)
            
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
           
            # affine transform (scale, rotation)
            shift_y, shift_x = np.array(img.shape[:2]) / 2.
            shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
            shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
            r_angle = random.randint(0, 359)
            scale_factor = 1.25
            scale = random.uniform(1. / scale_factor, scale_factor)
            tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
            img = warp(img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img_color = np.copy(img)
        else:
            img_color = np.copy(img)
      
        if normalize == "instance_std":
            means = np.zeros(3)
            stds = np.array([255.0, 255.0, 255.0])
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
                    std_val = np.std(img_color[..., i][img_color[..., i] > 10])
                    stds[i] = std_val if std_val > 0 else 255
            images_arr[file_index] = (img - means) / stds
        elif normalize == "normalize":
            images_arr[file_index] = 1.*img / 255.0
        else:
            images_arr[file_index] = img.astype(np.float32)
    return images_arr


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def print_stats(y_true, y_pred):
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    HM = 2 * spe * sen / (spe + sen)
    AUC_ROC = roc_auc_score(y_true, y_pred)
    
    print(cm)
    print("specificity : {},  sensitivity : {}, harmonic mean : {},  ROC_AUC : {} ".format(spe, sen, HM, AUC_ROC))   


def get_metric(metrics, y_true, y_pred):
    values = []
    for metric in metrics:
        if metric == "HM":
            cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
            spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
            sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
            HM = 2 * spe * sen / (spe + sen)
            values.append(HM)
        elif metric == "auroc":
            AUC_ROC = roc_auc_score(y_true, y_pred)
            values.append(AUC_ROC)

    return values


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        images_arr[file_index] = np.array(Image.open(filenames[file_index]))
    
    return images_arr


def normalize(imgs):
    images_arr = np.copy(1.*imgs / 255.0)
    return images_arr


def resize_img(img, target_size):
    if len(img.shape) == 4:  # (n,h,w,d)
        n, h, w, d = img.shape
        h_to, w_to = target_size
        if h == h_to and w == w_to:
            return img
        else:
            resized_img = np.zeros((n, target_size[0], target_size[1], d))
            for index in range(n):
                resized_img[index, ...] = imresize(img[index, ...], target_size, 'bilinear')
            return resized_img
    elif len(img.shape) == 3:  # (h,w,d)
        assert len(img.shape) == 2
        h_from, w_from = img.shape
        h_to, w_to = target_size
        if h_from == h_to and w_from == w_to:
            return img
        else:
            return zoom(img, (1.*h_to / h_from, 1.*w_to / w_from), order=2)


def z_score(imgs):
    images_arr = np.zeros(imgs.shape)
    for index in range(imgs.shape[0]):
        img = np.copy(imgs[index, ...])
        img[img < 10] = 0
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                stds[i] = std_val if std_val > 0 else 255
        images_arr[index] = (img - means) / stds
    return images_arr


def plot_imgs(imgs, out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for i in range(imgs.shape[0]):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(os.path.join(out_dir, "imgs_{}.png".format(i + 1)))
        Image.fromarray(imgs[i, ::32, ::32].astype(np.uint8)).save(os.path.join(out_dir, "imgs_discrete_{}.png".format(i + 1)))
        resized_img = imresize(imgs[i, ...], (16, 16), 'bilinear')
        Image.fromarray(resized_img.astype(np.uint8)).save(os.path.join(out_dir, "imgs_resized_{}.png".format(i + 1)))


class Scheduler:

    def __init__(self, schedules, init_lr):
        self.schedules = schedules
        self.lr = init_lr

    def get_lr(self):
        return self.lr
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr']:
            self.lr = self.schedules['lr'][key]
