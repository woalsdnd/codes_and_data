import pandas as pd
import numpy as np
import os
from subprocess import Popen, PIPE
import utils

thresh_interval = 0.1

from_dir = "/home/woalsdnd/data/preprocessed/kaggle_messidor_IDRiD_REFUGE_riga/"
to_dir = "/home/woalsdnd/palm_competition_2019/classification/FN_candidate"
df_result = pd.read_csv("/home/woalsdnd/palm_competition_2019/results/classification/submit.csv")
for index_thresh in range(1, 10):
    # get filenames
    lower_bound = index_thresh * thresh_interval
    upper_bound = (index_thresh + 1) * thresh_interval
    fnames = list(df_result[(df_result["PM prob"] >= lower_bound) & (df_result["PM prob"] < upper_bound)]["FileName"])
    
    # copy the files
    save_dir = os.path.join(to_dir, "{}".format(upper_bound))
    utils.mkdir_if_not_exist(save_dir)
    for fname in fnames:
        pipes = Popen("cp {} {}".format(os.path.join(from_dir, fname), save_dir), shell=True, stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()
