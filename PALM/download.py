import os
from subprocess import Popen, PIPE

# target_path = "pm_mined_all"
target_path = "ambiguous"

fnames = os.listdir(target_path)
for fname in fnames:
    # set fname for the other eye
    if "right" in fname:
        other_fname=fname.replace("right","left")
    elif "left" in fname:
        other_fname=fname.replace("left","right")
    else:
        continue
        
    # download the other eye if not exists
    other_fpath=os.path.join(target_path, other_fname)
    if not os.path.exists(other_fpath):
        pipes = Popen(["scp", "server10:/home/woalsdnd/data/preprocessed/kaggle_messidor_IDRiD_REFUGE_riga/{}".format(other_fname), target_path], stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()
