# python train.py --gpu_index=2 --batch_size=28 --load_model_save_dir=../models/classification/20190118/
import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator
import models
import label_provider
import label_provider_FN_care
import numpy as np
from keras.models import model_from_json

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=int,
    required=True
    )
parser.add_argument(
    '--load_model_save_dir',
    type=str,
    required=False,
    default=None
    )
FLAGS, _ = parser.parse_known_args()

# set misc params
n_epochs = 100
# schedules = {'lr':{'0': 0.001, '100':0.0001, '200':0.00001}}
schedules = {'lr':{'0': 0.0001, '10':0.00003}}
model_save_dir = "../models/classification/20190306_AUROC1_mined_rerun"
input_check_dir = "../input_checks/classification"
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)
if not os.path.isdir(model_save_dir):
    os.makedirs(model_save_dir)

# training settings
# train_filenames_labels, val_filenames_labels = label_provider.labels(init_run=True)
# train_filenames_labels, val_filenames_labels = label_provider_FN_care.labels(init_run=True)
# train_filenames_labels, val_filenames_labels = label_provider.labels(init_run=False)
train_filenames_labels, val_filenames_labels = label_provider.labels(init_run=False, use_val=True, use_mined_label=True, use_train=True)
weight = utils.class_weight(train_filenames_labels)
train_batch_fetcher = iterator.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, "normalize")
validation_batch_fetcher = iterator.ValidationBatchFetcher(val_filenames_labels, FLAGS.batch_size, "normalize")

# create & save network
img_shape = (512, 512, 3)
if FLAGS.load_model_save_dir:
    network_file = utils.all_files_under(FLAGS.load_model_save_dir, extension=".json")
    weight_file = utils.all_files_under(FLAGS.load_model_save_dir, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network = models.set_optimizer(network)
    print "model loaded from files"
else:
    network = models.network_activation_out(img_shape)
    print "model from the scratch"
network.summary()
with open(os.path.join(model_save_dir, "network.json"), 'w') as f:
    f.write(network.to_json())

# train the network 
scheduler = utils.Scheduler(schedules, schedules['lr']['0'])
check_train_batch, check_test_batch = True, True
auroc_best = 0
for epoch in range(n_epochs):
    # update step sizes, learning rates, lambda
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())    
    
    # train on train set
    training_loss = {"loss_class":[], "acc_class":[]}
    start_time = time.time()
    for filenames, batch_x, batch_y in train_batch_fetcher():
        if check_train_batch:
            utils.plot_imgs((batch_x * 255), os.path.join(input_check_dir, "train", "imgs"))
            check_train_batch = False
        loss_class, acc_class = network.train_on_batch(batch_x, batch_y)
        training_loss["loss_class"] += [loss_class] * len(filenames)
        training_loss["acc_class"] += [acc_class] * len(filenames)
    print ("classification loss: {}".format(np.mean(training_loss["loss_class"])))
    print ("classification acc: {}".format(np.mean(training_loss["acc_class"])))
    
    # evaluate on val set
    true_labels, pred_labels = [], []
    for filenames, batch_x, batch_y in validation_batch_fetcher():
        if check_test_batch:
            utils.plot_imgs(batch_x, os.path.join(input_check_dir, "val", "imgs"))
            check_test_batch = False
        preds = network.predict(batch_x)
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
    utils.print_stats(true_labels, pred_labels)
    HM_curr, auroc_curr = utils.get_metric(["HM", "auroc"], true_labels, pred_labels)
    duration = time.time() - start_time
    print ("{}th epoch ==> duration : {}".format(epoch, duration))
    
    # save weights
    network.save_weights(os.path.join(model_save_dir, "weight_{}epoch.h5".format(epoch)))
    if auroc_curr > auroc_best:
        auroc_best = auroc_curr
        HM_at_best_auroc = HM_curr
        network.save_weights(os.path.join(model_save_dir, "weight_best_auroc.h5"))
    sys.stdout.flush()
print ("best auroc : {}, HM at best auroc : {}".format(auroc_best, HM_at_best_auroc))
