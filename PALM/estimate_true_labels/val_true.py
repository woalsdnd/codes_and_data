import pandas as pd
import numpy as np
from sklearn.metrics.ranking import roc_auc_score
import sys

df = pd.read_csv("Classification_Results.csv")
df_test = pd.read_csv("/home/woalsdnd/palm_competition_2019/results/classification/offsite_submission/20190306_AUROC1_mined.csv")
pred = df_test["PM Risk"]

list_ambig = ["V0009.jpg", "V0013.jpg", "V0030.jpg", "V0043.jpg", "V0062.jpg", "V0072.jpg", "V0086.jpg", "V0111.jpg", "V0115.jpg",
            "V0191.jpg", "V0193.jpg", "V0203.jpg", "V0207.jpg", "V0241.jpg", "V0275.jpg", "V0284.jpg", "V0291.jpg", "V0308.jpg",
            "V0323.jpg", "V0331.jpg", "V0348.jpg", "V0368.jpg", "V0373.jpg"]

list_cases=[1142271,1145853,1305327,1427967,1436407,1437421,1440237,1686013,1686510,1696759,1697023,1697773,1699535,1700605,1702351]
for case in list_cases:
    df_copy = df.copy()
    token = np.array(list(np.binary_repr(case).zfill(23))).astype(np.int8)
    for index, ambig in enumerate(list_ambig):
        df_copy.loc[df_copy["FileName"] == ambig, "PM Risk"] = token[index]
    
    df_copy.loc[:, "PM Risk"] = df_copy["PM Risk"].map(lambda x:np.round(x))
    df_copy.loc[:, "filename"] = df_copy["FileName"]
    df_copy.loc[:, "label"] = df_copy["PM Risk"]
    df_copy = df_copy[["filename", "label"]]
    # df_copy.to_csv("val.csv", index=False)
    auroc = roc_auc_score(df_copy["label"], pred)
    print auroc
    
