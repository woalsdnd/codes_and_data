import pandas as pd
import utils
import os

train_data_path = "../../data/preprocessed/kaggle_messidor_IDRiD_REFUGE_riga"
val_data_path = "../../data/preprocessed/PALM-Training400/"
offline_data_path = "../../data/preprocessed/PALM-Validation400/"
train_pos_data_path1 = "pm_mined"
train_pos_data_path2 = "pm_web_preprocessed"
train_pos_data_path3 = "FN_target"
train_data_to_exclude_path = "imgs_to_exclude"
mined_label = "estimate_true_labels/val.csv"


def labels(init_run=False, use_val=False, use_mined_label=False, use_train=False):
    # validation labels
    val_fnames = utils.all_files_under(val_data_path, append_path=False)
    val_fpaths, val_labels = [], []
    for val_fname in val_fnames:
        val_fpaths.append(os.path.join(val_data_path, val_fname))
        if "P" in val_fname:
            val_labels.append(1)
        else:
            val_labels.append(0)
    
    if use_val:
        # generate dataframes
        df_train = pd.DataFrame({'filename': val_fpaths, 'label':val_labels})
        df_val = pd.DataFrame({'filename': val_fpaths, 'label':val_labels})
        if use_mined_label:
            df_mined = pd.read_csv(mined_label)
            df_mined.loc[:, "filename"] = df_mined["FileName"].map(lambda x:os.path.join(offline_data_path, x.replace(".jpg", ".png")))
            df_train = pd.concat([df_train, df_mined], ignore_index=True)
#             df_val = df_mined
#             df_val = pd.concat([df_val, df_mined], ignore_index=True)
    if use_train:
        # training labels
        train_paths = utils.all_files_under(train_data_path)
        excluding_fnames = utils.all_files_under(train_data_to_exclude_path, append_path=False)
        train_paths = [train_path for train_path in train_paths if os.path.basename(train_path) not in excluding_fnames]
        train_pos_paths = utils.all_files_under(train_pos_data_path1) + utils.all_files_under(train_pos_data_path2) + utils.all_files_under(train_pos_data_path3)
        if init_run:
            for index, val_fpath in enumerate(val_fpaths):
                if val_labels[index] == 1:
                    train_pos_paths.append(val_fpath)
        
        # set paths and labels
        train_pos_fnames = [os.path.basename(fpath) for fpath in train_pos_paths]
        train_neg_paths = [train_path for train_path in train_paths if os.path.basename(train_path) not in train_pos_fnames]
        train_paths = train_neg_paths + train_pos_paths
        train_labels = [0] * len(train_neg_paths) + [1] * len(train_pos_paths)
        print("open data: pos-{}, neg-{}").format(len(train_pos_paths), len(train_neg_paths))
    
        # generate dataframes
        if "df_train" not in locals() :
            df_train = pd.DataFrame({'filename': train_paths, 'label':train_labels})
        else:
            df_train_additional = pd.DataFrame({'filename': train_paths, 'label':train_labels})
            df_train = pd.concat([df_train, df_train_additional], ignore_index=True)
        if "df_val" not in locals() :
            df_val = pd.DataFrame({'filename': val_fpaths, 'label':val_labels})
    
    return df_train, df_val
