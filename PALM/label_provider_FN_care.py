import pandas as pd
import utils
import os

train_data_path = "../../data/preprocessed/kaggle_messidor_IDRiD_REFUGE_riga"
val_data_path = "../../data/preprocessed/PALM-Training400/"
train_pos_data_path = "FN_target"
train_data_to_exclude_path = "imgs_to_exclude"
list_pos = ['P0058.png', 'P0061.png', 'P0062.png', 'P0070.png', 'P0076.png', 'P0078.png',
           'P0134.png', 'P0171.png', 'P0184.png']


def labels(init_run=True):
    # validation labels
    val_fnames = utils.all_files_under(val_data_path, append_path=False)
    val_fpaths, val_labels = [], []
    for val_fname in val_fnames:
        val_fpaths.append(os.path.join(val_data_path, val_fname))
        if val_fname in list_pos:
            val_labels.append(1)
        else:
            val_labels.append(0)
    
    # training labels
    train_paths = utils.all_files_under(train_data_path)
    excluding_fnames = utils.all_files_under(train_data_to_exclude_path, append_path=False)
    train_paths = [train_path for train_path in train_paths if os.path.basename(train_path) not in excluding_fnames]
    train_pos_paths = utils.all_files_under(train_pos_data_path)
    if init_run:
        for index, val_fpath in enumerate(val_fpaths):
            if val_labels[index] == 1:
                train_pos_paths.append(val_fpath)
    
    # set paths and labels
    train_pos_fnames = [os.path.basename(fpath) for fpath in train_pos_paths]
    train_neg_paths = [train_path for train_path in train_paths if os.path.basename(train_path) not in train_pos_fnames]
    train_paths = train_neg_paths + train_pos_paths
    train_labels = [0] * len(train_neg_paths) + [1] * len(train_pos_paths)
    print("train data: pos-{}, neg-{}").format(len(train_pos_paths), len(train_neg_paths))

    # generate dataframes
    df_train = pd.DataFrame({'filename': train_paths, 'label':train_labels})
    df_val = pd.DataFrame({'filename': val_fpaths, 'label':val_labels})
    
    return df_train, df_val
