import numpy as np
import pandas as pd
import argparse
from sklearn.metrics import roc_auc_score

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_csv',
    type=str,
    required=False
    )
FLAGS, _ = parser.parse_known_args()

pred = pd.read_csv(FLAGS.pred_csv)
true = pd.read_csv("estimate_true_labels/val.csv")

print pred
print true

# auroc_score = roc_auc_score(np.array(true["label"]), pred["PM prob"])
auroc_score = roc_auc_score(np.array(true["label"]), pred["PM Risk"])
print("AUROC SCORE : {}".format(auroc_score))