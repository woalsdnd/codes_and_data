# USAGE
# python inference.py  --gpu_index=1 --task=validation --model_path=./models/classification/20190118/
# python inference.py  --gpu_index=1 --task=mining --model_path=../models/classification/ambiguous_pm_set_to_0
import utils
import os
import argparse
import time
import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score


def get_mean_pred(n_img_aug, network, img):
    assert len(img) == 1
    list_pred = []
    for _ in range(n_img_aug):
        aug_resized_img = np.expand_dims(utils.augment_img(img[0, ...]), axis=0)
        normalized_resized_img = utils.normalize(aug_resized_img)
        pred = network.predict(normalized_resized_img)
        list_pred.append(pred[0, 0])
    return np.mean(list_pred)


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--task',
    type=str,
    required=True
    )
parser.add_argument(
    '--model_path',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set misc params (gpu index, paths for results and img analysis) 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
out_dir = "/home/woalsdnd/palm_competition_2019/results/classification/csv_files"
utils.mkdir_if_not_exist(out_dir)
    
# load a network
model = FLAGS.model_path
network = utils.load_network(model)

# merge image in two directories
if FLAGS.task == "validation":
    img_dir = "/home/woalsdnd/data/preprocessed/PALM-Training400"
elif FLAGS.task == "mining":
    img_dir = "/home/woalsdnd/data/preprocessed/kaggle_messidor_IDRiD_REFUGE_riga"
elif FLAGS.task == "offsite":
    img_dir = "/home/woalsdnd/data/preprocessed/PALM-Validation400"
filepaths = utils.all_files_under(img_dir)
    
# run inference
start_time = time.time()
gts, preds = [], []
fns = []
target_size = (512, 512)
for filepath in filepaths:
    # load img
    fn = os.path.basename(filepath)
    fns.append(fn)
    print ("processing {}...".format(fn))
    
    # preprocess img 
    img = utils.imagefiles2arrs([filepath])
    resized_img = utils.resize_img(img, target_size)
    resized_img[resized_img < 10] = 0
    normalized_resized_img = utils.normalize(resized_img)

    # run network
    pred = network.predict(normalized_resized_img)
    preds.append(pred[0, 0])
#     pred = get_mean_pred(10, network, resized_img)
#     preds.append(pred)
    if FLAGS.task == "validation":
        gt = 1 if "P" in filepath.split("/")[-1].split(".")[0] else 0
        gts.append(gt)
    
# save to csv
df = pd.DataFrame({"FileName":[fn.replace(".png", ".jpg") for fn in fns], "PM Risk":preds})
df.to_csv(os.path.join(out_dir, "{}.csv".format(os.path.basename(FLAGS.model_path))), index=False)

# evaluate performance for validation
if FLAGS.task == "validation":
    gts, preds, fns = np.array(gts), np.array(preds), np.array(fns)
    print ("false positive: {}".format(fns[(gts == 0) & (preds >= 0.5)]))
    print ("false negative: {}".format(fns[(gts == 1) & (preds < 0.5)]))
    auroc_score = roc_auc_score(gts, preds)
    print ("AUROC SCORE : {}".format(auroc_score))
    duration = time.time() - start_time
    print ("duration : {}".format(duration))
