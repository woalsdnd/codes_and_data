# Leveraging the Generalization Ability of Deep Convolutional Neural Networks for Enhancing Classifiers for Color Fundus Photographs #

## CAVEATE ##
To run the codes, download fundus images from Kaggle 2015, PALM, RIGA, MESSIDOR, REFUGE, IDRiD, and crop-resize the original images.

## Package Dependency ##
pandas==0.16.0  
Keras==2.0.8  
scikit_image==0.13.0  
tensorflow==1.6.0rc0  
numpy==1.14.4  
Pillow==5.4.1  
SharedArray==3.1.0  
scikit-image==0.10.0  
scikit_learn==0.20.3  

## How to Run Codes ##
**Crop - Resize Images**
```
python crop_resize.py  --img_dir=<str> --out_dir=<str>  --n_processes=<int> --w_target=512 --h_target=512 --extension=<str>
```
### Kaggle - Referrable Diabetic Retinopathy ###
**Our Method**
```
python train_filtering_suspicious_data.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```
**baseline**
```
python train.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```
**Bootstrap [[1]](#1)**
```
python train_bootstrap.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```
**S-model[[2]](#2)**
```
python train_smodel.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```
**Joint-training[[3]](#3)**
```
python train_smodel.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```
### PALM - Pathological Myopia ###
```
python train.py --gpu_index=<int> --noise_ratio=<float> --img_path=<str>
```

## File Hierarchy ##
```
.  
├── requirements.txt  
├── PALM  
│   ├── utils.py  
│   ├── train.py  
│   ├── split.py  
│   ├── pm_web_preprocessed  
│   ├── pm_mined  
│   ├── pm_ambiguous  
│   ├── ori_data  
│   ├── models.py  
│   ├── label_provider_FN_care.py  
│   ├── label_provider.py  
│   ├── iterator.py  
│   ├── inference.py  
│   ├── imgs_to_exclude  
│   ├── evaluation.py  
│   ├── estimate_true_labels  
│   ├── download.py  
│   ├── crop_resize.py  
│   └── FN_target  
└── Kaggle  
    ├── train_smodel.py  
    ├── train_joint_training.py  
    ├── train_filtering_suspicious_data.py  
    ├── train_bootstrap.py  
    ├── trainLabels.csv  
    ├── train.py  
    ├── testLabels.csv  
    ├── models.py  
    └── iterator.py  
```

## References
1. [Reed, Scott, et al. *Training deep neural networks on noisy labels with bootstrapping* arXiv preprint arXiv:1412.6596* (2014)](https://arxiv.org/pdf/1412.6596.pdf)
2. [Goldberger, Jacob, and Ehud Ben-Reuven. *Training deep neural-networks using a noise adaptation layer* (2016)](https://openreview.net/pdf?id=H12GRgcxg)
3. [Tanaka, Daiki, et al. *Joint optimization framework for learning with noisy labels* Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition (2018)](http://openaccess.thecvf.com/content_cvpr_2018/papers/Tanaka_Joint_Optimization_Framework_CVPR_2018_paper.pdf)

## LICENSE ##
This is under the MIT License  
Copyright (c) 2020 Vuno Inc. (www.vuno.co)